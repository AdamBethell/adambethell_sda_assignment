//
//  DraggablePoint.hpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 03/01/2018.
//
//

#ifndef DraggablePoint_hpp
#define DraggablePoint_hpp

#include "../JuceLibraryCode/JuceHeader.h"
/**
 Base class for draggable elements.
 */
class DraggablePoint : public Component
{
public:
    /**
     Container structure for the colours used in this draggable element.
     */
    struct PointColours
    {
        PointColours (Colour _outerColourWhenHilighted,
                      Colour _innerColourWhenHilighted,
                      Colour _outerColourWhenOff,
                      Colour _innerColourWhenOff,
                      Colour _outerColourWhenOver,
                      Colour _innerColourWhenOver) :
        outerColourWhenHilighted (_outerColourWhenHilighted),
        innerColourWhenHilighted (_innerColourWhenHilighted),
        outerColourWhenOff (_outerColourWhenOff),
        innerColourWhenOff (_innerColourWhenOff),
        outerColourWhenOver (_outerColourWhenOver),
        innerColourWhenOver (_innerColourWhenOver)
        {
            
        }
        Colour outerColourWhenHilighted;
        Colour innerColourWhenHilighted;
        
        Colour outerColourWhenOff;
        Colour innerColourWhenOff;
        
        Colour outerColourWhenOver;
        Colour innerColourWhenOver;
    };
    
    /**
     Constructor.
     */
    DraggablePoint (PointColours _colours, String _dragAndDropDescription, Array<DraggablePoint*>& _allPoints);
    /** 
     Destructor
     */
    ~DraggablePoint();
    
    
    
    /* Component */
    void paint (Graphics& g) override;
    void resized() override;
    
    void mouseDown (const MouseEvent& event) override;
    void mouseEnter (const MouseEvent& event) override;
    void mouseExit (const MouseEvent& event) override;
    
    /* Member functions */
    /**
     Sets the currently selected draggable element.
     This sets exclusive highlighting of all instances that share a class (does not include this class).
     For example, when a new DraggablePartial is selected, only one DraggablePartial is highlighted; this does not affect DraggableNode that also inherits from this class.
     
     @see DraggablePartial, DraggableNode
     */
    void setSelected();
    
    /**
     Returns true or false based on if the current mouse position is different to the mouse position when this point was last clicked.
     */
    bool hasMoved();
    
    /* Pure Virtual members */
    /**
     Child classes override this function to draw the inner part of the node.
     @see DraggablePartial, DraggableNode
     */
    virtual void drawInner (Graphics& g) = 0;
    /**
     Child classes override this function to draw the outer part of the node.
     @see DraggablePartial, DraggableNode
     */
    virtual void drawOuter (Graphics& g) = 0;
    
private:
    void unselectAll();
    void highlight();
    void unHighlight();
    
    PointColours colours;
    Colour innerColour;
    Colour outerColour;
    String dragAndDropDescription;
    
    bool highlighted;
    bool dragStarted;
    Point<int> mouseDownPosition;
    
    Array<DraggablePoint*>& allPoints;
};

#endif /* DraggablePoint_hpp */
