//
//  PartialRulerComponent.cpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 13/11/2017.
//
//

#include "PartialRulerComponent.hpp"

PartialRulerComponent::PartialRulerComponent()
{
    
}
PartialRulerComponent::~PartialRulerComponent()
{
    
}

void PartialRulerComponent::paint (Graphics& g)
{
    Rectangle<int> bounds = getLocalBounds();
    
    g.setColour (Colours::black);
    g.drawLine (bounds.getX(), bounds.getY() + bounds.getHeight(), bounds.getX() + bounds.getWidth(), bounds.getY() + bounds.getHeight(), 3);
    g.drawLine (bounds.getX(), bounds.getY() + bounds.getHeight(), bounds.getX(), bounds.getY(), 3);
}
