//
//  DraggablePartial.hpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 20/11/2017.
//
//

#ifndef DraggablePartial_hpp
#define DraggablePartial_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "../DraggablePoint.hpp"
/**
 A specialised dragganle point that is circular in appearence.
 */
class DraggablePartial : public DraggablePoint
{
public:
    /**
     Contructor.
     */
    DraggablePartial (int _id);
    /**
     Destructor.
     */
    ~DraggablePartial();
    
    void drawInner (Graphics& g) override;
    void drawOuter (Graphics& g) override;
    
    /**
     Returns the unique id of this DraggablePartial.
     Assigned in the contructor, the id matches with the id of a partial in the data. This is used to link the visuals to the model.
     */
    int getId() const { return id; }
private:

    int id;
    
    static Array<DraggablePoint*> allPartials;
};

#endif /* DraggablePartial_hpp */
