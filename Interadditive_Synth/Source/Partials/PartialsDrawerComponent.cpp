//
//  PartialsDrawerComponent.cpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 20/11/2017.
//
//

#include "PartialsDrawerComponent.hpp"

PartialsDrawerComponent::PartialsDrawerComponent (Array<PartialData>& partialArrayReference) :
listener (nullptr),
currentMultiplyer(0),
currentAmplitude(0),
lockToInt (false),
partialDataArray (partialArrayReference),
currentlySelectedId (-1)
{
    addAndMakeVisible (&ruler);
    ruler.addMouseListener (this, false);
        
    addAndMakeVisible (&toolTip);
    toolTip.setText ("", dontSendNotification);
    toolTip.setColour (Label::ColourIds::textColourId, Colours::black);
    toolTip.setJustificationType (Justification::topRight);
    toolTip.setVisible (false);
    toolTip.setFont (12);
    toolTip.setAlwaysOnTop (true);
    toolTip.setInterceptsMouseClicks (false, false);
    
    startTimer (25);
}
PartialsDrawerComponent::~PartialsDrawerComponent()
{
    
}

void PartialsDrawerComponent::paint (Graphics& g)
{
    g.setColour (Colour(0xFF008FFF));
    for (auto* component : partialVisuals)
    {
        float xPos = component->getPosition().getX() + ((float)component->getWidth() / 2.0);
        float startY = component->getPosition().getY() + component->getHeight() - 1;
        g.drawLine (xPos, startY, xPos, getHeight(), 3);
    }
    
}
void PartialsDrawerComponent::resized()
{
    Rectangle<int> bounds = getLocalBounds();
    
    ruler.setBounds (bounds);
}

void PartialsDrawerComponent::deleteSelectedPartial()
{
    DBG ("deleteSelectedPartial()");
    for (int i = 0; i < partialDataArray.size(); i++)
    {
        if (partialDataArray.getReference (i).getId() == currentlySelectedId)
        {
            DBG ("data found");
            partialDataArray.remove (i);
        }
    }
    
    for (int i = 0; i < partialVisuals.size(); i++)
    {
        if (partialVisuals[i]->getId() == currentlySelectedId)
        {
            DBG ("visual found");
            partialVisuals.remove (i);
        }
    }
    
    if (partialDataArray.size() == 0)
    {
        createPartialAtMousePosition (Point<int> (0,0));
    }
    
    int newlySelected = partialDataArray.getReference (0).getId();
    
    for (int i = 0; i < partialVisuals.size(); i++)
    {
        if (partialVisuals[i]->getId() == currentlySelectedId)
        {
            partialVisuals[i]->setSelected();
        }
        
    }
    
    
    if (listener != nullptr)
        listener->selectedPartialChanged (newlySelected);
    
    repaint();
}

void PartialsDrawerComponent::timerCallback()
{
    Point<int> point = getMouseXYRelative();
    if (point.x < 0 || point.y < 0 || point.x > getWidth() || point.y > getHeight())
    {
        toolTip.setVisible (false);
    }
    else
    {
        toolTip.setVisible (true);
        
        currentAmplitude = mousePositionToAmplitude (point);
        currentMultiplyer = mousePositionToMultiplyer (point, lockToInt);
        
        toolTip.setText (
                         "mult: " + String(currentMultiplyer) + "\n" +
                         "amp: " + String(currentAmplitude),
                         dontSendNotification
                         );
        toolTip.setBounds (point.x-70, point.y-25, 140, 50);
        toolTip.setJustificationType (getJustificationForTooltip (point));
    }
}
void PartialsDrawerComponent::mouseDown (const MouseEvent& event)
{
}
void PartialsDrawerComponent::mouseUp (const MouseEvent& event)
{
    createPartialAtMousePosition (getMouseXYRelative());
}

void PartialsDrawerComponent::createPartialAtMousePosition (Point<int> position, bool onlyCreateVisual, int idToAssign)
{
    //    DBG (
    //         "Mult: " + String (mousePositionToMultiplyer (getMouseXYRelative(), lockToInt)) +
    //         "\nAmp: " + String (mousePositionToAmplitude (getMouseXYRelative()))
    //         );
    DraggablePartial* newPartial = nullptr;
    if (! onlyCreateVisual)
    {
        partialDataArray.add (
                              PartialData (
                                           mousePositionToMultiplyer (position, lockToInt),
                                           mousePositionToAmplitude (position)
                                           )
                              );
        
        currentlySelectedId = partialDataArray.getLast().getId();
        
        if (listener != nullptr)
            listener->selectedPartialChanged (currentlySelectedId);
        
        newPartial = new DraggablePartial (currentlySelectedId);
    }
    else
    {

        newPartial = new DraggablePartial (idToAssign);
    }
    
    partialVisuals.add (newPartial);
    addAndMakeVisible (newPartial);
    newPartial->setBounds (position.x-10, position.y-10, 20, 20);
    
    Point<int> point;
    point.setY (position.getY());
    point.setX (multiplyerToMousePositionX (mousePositionToMultiplyer (position, lockToInt)));
    newPartial->setCentrePosition (point);
    
    repaint();
}

bool PartialsDrawerComponent::isInterestedInDragSource (const SourceDetails& dragSourceDetails)
{
    if (dragSourceDetails.description == "Partial") return true;
    
    return false;
}
void PartialsDrawerComponent::dragOperationStarted (const SourceDetails& dragSourceDetails)
{
    DraggablePartial* component = dynamic_cast<DraggablePartial*> (dragSourceDetails.sourceComponent.get());
    currentlySelectedId = component->getId();
    if (listener != nullptr)
        listener->selectedPartialChanged (currentlySelectedId);
}
void PartialsDrawerComponent::itemDropped (const SourceDetails& dragSourceDetails)
{
    DraggablePartial* component = dynamic_cast<DraggablePartial*> (dragSourceDetails.sourceComponent.get());
    if (component->hasMoved())
    {
        Point<int> point;
        point.setY (dragSourceDetails.localPosition.getY());
        point.setX (multiplyerToMousePositionX (mousePositionToMultiplyer (getMouseXYRelative(), lockToInt)));
        component->setCentrePosition (point);
        
        for (int i = 0; i < partialDataArray.size(); i++)
        {
            if (partialDataArray.getReference (i).getId() == component->getId())
            {
                partialDataArray.getReference (i).setAmp (mousePositionToAmplitude (point));
                partialDataArray.getReference (i).setMult (mousePositionToMultiplyer (point, lockToInt));
            }
        }
        
        repaint();
    }
}

Justification PartialsDrawerComponent::getJustificationForTooltip (Point<int> point)
{
    bool isLeft = false;
    bool isTop = true;
    if (point.x + 60 > getLocalBounds().getWidth()) isLeft = true;
    if (point.y - 60 < 0) isTop = false;
    
    if (isLeft && isTop) return Justification::topLeft;
    else if (!isLeft && isTop) return Justification::topRight;
    else if (isLeft && !isTop) return Justification::bottomLeft;
    else if (!isLeft && !isTop) return Justification::bottomRight;
    
    jassertfalse;
    return Justification::topLeft;
}

float PartialsDrawerComponent::mousePositionToMultiplyer (Point<int> point, bool intLock)
{
    float mult = (float)(getWidth()-point.x)/(float)(getWidth());
    mult *= 19;
    mult = 19 - mult;
    mult += 1;
    
    if (intLock)
    {
        mult += 0.5;
        mult = (int)mult;
    }
    else
    {
        mult = (int)(mult * 1000);
        mult /= 1000.0;
    }
    
    return mult;
}
float PartialsDrawerComponent::mousePositionToAmplitude (Point<int> point)
{
    float amp = (float)(getHeight()-point.y)/(float)(getHeight());
    amp = (int)(amp * 1000);
    amp /= 1000.0;
    
    return amp;
}
float PartialsDrawerComponent::multiplyerToMousePositionX (float multiplyer)
{
    multiplyer -= 1;
    multiplyer /= 19;
    
    return multiplyer * getWidth();
}
float PartialsDrawerComponent::amplitudeToMousePositionY (float amplitude)
{
    return (float)getHeight() - ((float)getHeight() * amplitude);
}
void PartialsDrawerComponent::reload()
{
    partialVisuals.clear();
    
    for (auto partial : partialDataArray)
    {
        Point<int> position;
        DBG ("Placing partial at [" + String (partial.getMult()) + ", " + String (partial.getAmp()) + "]");
        position.setX (multiplyerToMousePositionX (partial.getMult()));
        position.setY (amplitudeToMousePositionY (partial.getAmp()));
        
        createPartialAtMousePosition (position, true, partial.getId());
    }
    
    currentlySelectedId = partialDataArray.getLast().getId();
    if (listener != nullptr)
        listener->selectedPartialChanged (currentlySelectedId);
}
