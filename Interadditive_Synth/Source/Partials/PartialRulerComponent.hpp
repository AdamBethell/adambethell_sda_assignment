//
//  PartialRulerComponent.hpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 13/11/2017.
//
//

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

/**
 Displays the ruler lines in the PartialsComponent.
 Overrides the paint function to draw the ruler lines.
 */
class PartialRulerComponent : public Component
{
public:
    PartialRulerComponent();
    ~PartialRulerComponent();
    
    /* Component */
    void paint (Graphics& g) override;
private:
};
