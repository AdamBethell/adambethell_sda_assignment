//
//  PartialsComponent.cpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 13/11/2017.
//
//

#include "PartialsComponent.hpp"

PartialsComponent::PartialsComponent (Array<PartialData>& partialArrayReference) :
listener (nullptr),
partialsDrawer (partialArrayReference)
{
    addAndMakeVisible (&label);
    label.setText ("Partials", dontSendNotification);
    label.setColour (Label::ColourIds::textColourId, Colours::black);
    label.setJustificationType (Justification::topLeft);
    
    addAndMakeVisible (&lockMultToInt);
    lockMultToInt.setButtonText ("Snap to harmonics");
    lockMultToInt.setColour (ToggleButton::ColourIds::textColourId, Colours::black);
    lockMultToInt.setColour (ToggleButton::ColourIds::tickColourId, Colours::black);
    lockMultToInt.setColour (ToggleButton::ColourIds::tickDisabledColourId, Colours::black);
    lockMultToInt.addListener (this);
    lockMultToInt.setToggleState (true, sendNotification);
    
    addAndMakeVisible (&deletePartialButton);
    deletePartialButton.setButtonText (CharPointer_UTF8 ("\xe2\x8c\xab"));
    deletePartialButton.addListener (this);
    
    addAndMakeVisible (&partialsDrawer);
    partialsDrawer.setListener (this);
}
PartialsComponent::~PartialsComponent()
{
    
}

void PartialsComponent::paint (Graphics& g)
{
    g.fillAll (Colour (0xFFCDECFC));
}
void PartialsComponent::resized()
{
    Rectangle<int> bounds = getLocalBounds();
    Rectangle<int> uiArea = bounds.removeFromLeft (60);
    
    label.setBounds (bounds);
    partialsDrawer.setBounds (bounds.reduced (15));
    
    lockMultToInt.setBounds (uiArea.removeFromTop (20));
    uiArea.removeFromTop (20);
    deletePartialButton.setBounds (uiArea.removeFromTop (50));
}

void PartialsComponent::buttonClicked (Button* button)
{
    if (button == &lockMultToInt)
    {
        partialsDrawer.setLockToInt (button->getToggleState());
    }
    else if (button == &deletePartialButton)
    {
        DBG ("buttonClicked()");
        partialsDrawer.deleteSelectedPartial();
    }
}

void PartialsComponent::selectedPartialChanged (int idOfSelected)
{
    if (listener != nullptr)
        listener->selectedPartialChanged (idOfSelected);
}

void PartialsComponent::reload()
{
    partialsDrawer.reload();
}
