//
//  PartialsDrawerComponent.hpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 20/11/2017.
//
//

#ifndef PartialsDrawerComponent_hpp
#define PartialsDrawerComponent_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "PartialRulerComponent.hpp"
#include "DraggablePartial.hpp"
#include "../SimpleStructs.hpp"

/**
 Draws the partial visuals which can then be interacted with.
 */
class PartialsDrawerComponent :     public Component,
                                    public Timer,
                                    public DragAndDropContainer,
                                    public DragAndDropTarget
{
public:
    /**
     Constructor.
     */
    PartialsDrawerComponent (Array<PartialData>& partialArrayReference);
    /**
     Destructor.
     */
    ~PartialsDrawerComponent();

    /* Component */
    void paint (Graphics& g) override;
    void resized() override;
    void mouseDown (const MouseEvent& event) override;
    void mouseUp (const MouseEvent& event) override;
    
    /* Timer */
    void timerCallback() override;
    
    /* DragAndDropTarget */
    bool isInterestedInDragSource (const SourceDetails& dragSourceDetails) override;
    void itemDropped (const SourceDetails& dragSourceDetails) override;
    void dragOperationStarted (const SourceDetails& dragSourceDetails) override;
    
    /* Member functions */
    /**
     Deletes the selected partial.
     */
    void deleteSelectedPartial();
    /**
     Sets if newly added partials are restricted to being harmonics.
     If set true new partials will snap to a whole number value (representing a harmonic relation to the fundimental).
     If set false new partials will be placed as they are. This allows for numbers between integers (representing inharmonic partials).
     */
    void setLockToInt (bool shouldLock) { lockToInt = shouldLock; }
    Array<PartialData>& getPartialData() { return partialDataArray; }
    
    /**
     Redraws the partial visuals based on the partial data.
     */
    void reload();
    
    /* Listener */
    /**
     Used to receive callbacks when the user selects a new partial.
     */
    class Listener
    {
    public:
        virtual ~Listener(){}
        /**
         Changes the currently selected partial.
         Often used to update envelope views.
         */
        virtual void selectedPartialChanged (int idOfSelected) = 0;
    };
    /**
     Sets the listener.
     @see selectedPartialChanged
     */
    void setListener (Listener* listener_) { listener = listener_; }
private:
    Listener* listener;
    float mousePositionToMultiplyer (Point<int> point, bool intLock);
    float mousePositionToAmplitude (Point<int> point);
    float multiplyerToMousePositionX (float multiplyer);
    float amplitudeToMousePositionY (float amplitude);
    void createPartialAtMousePosition (Point<int> position, bool onlyCreateVisual = false, int idToAssign = -1);
    
    Justification getJustificationForTooltip (Point<int> point);
    float currentMultiplyer;
    float currentAmplitude;
    PartialRulerComponent ruler;
    Label toolTip;
    bool lockToInt;
    Array<PartialData>& partialDataArray;
    OwnedArray<DraggablePartial> partialVisuals;
    
    int currentlySelectedId;
    
};

#endif /* PartialsDrawerComponent_hpp */
