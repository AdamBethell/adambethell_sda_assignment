//
//  PartialsComponent.hpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 13/11/2017.
//
//

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "PartialsDrawerComponent.hpp"

/**
 A component that displays and edits partial point data.
 */
class PartialsComponent :   public Component,
                            public Button::Listener,
                            public PartialsDrawerComponent::Listener
{
public:
    /**
     Constructor.
     */
    PartialsComponent (Array<PartialData>& partialArrayReference);
    /**
     Destructor.
     */
    ~PartialsComponent();
    
    /* Component */
    void paint (Graphics& g) override;
    void resized() override;
    
    /* Button::Listener */
    void buttonClicked (Button* button) override;
    
    /* PartialsDrawerComponent::Listener */
    void selectedPartialChanged (int idOfSelected) override;
    
    /* Memebers */
    /**
     Calls PartialsDrawerComponent to redraw the partial visuals based on the partial data.
     @see PartialsDrawerComponent
     */
    void reload();
    
    /* Listener */
    /**
     Used to receive callbacks when the user selects a new partial.
     */
    class Listener
    {
    public:
        virtual ~Listener(){}
        /**
         Changes the currently selected partial.
         Often used to update envelope views.
         */
        virtual void selectedPartialChanged (int idOfSelected) = 0;
    };
    /**
     Sets the listener.
     @see selectedPartialChanged
     */
    void setListener (Listener* listener_) { listener = listener_; }
private:
    Listener* listener;
    PartialsDrawerComponent partialsDrawer;
    
    Label label;
    ToggleButton lockMultToInt;
    TextButton deletePartialButton;
};
