//
//  DraggablePartial.cpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 20/11/2017.
//
//

#include "DraggablePartial.hpp"

Array<DraggablePoint*> DraggablePartial::allPartials = Array<DraggablePoint*>();

DraggablePartial::DraggablePartial (int _id) :
DraggablePoint (DraggablePoint::PointColours (Colour (0xFFEE807F),
                                              Colour (0xFFD67BE8),
                                              Colour (0xFF008FFF),
                                              Colour (0xFF56F7CB),
                                              Colour (0xFF877FEE),
                                              Colour (0xFF56AFF7)),
                "Partial",
                allPartials),
id (_id)
{

}
DraggablePartial::~DraggablePartial()
{
    
}

void DraggablePartial::drawInner (Graphics& g)
{
    g.fillEllipse (getLocalBounds().toFloat().reduced (5));
}

void DraggablePartial::drawOuter (Graphics& g)
{
    g.drawEllipse (getLocalBounds().toFloat().reduced (2), 3);
}
