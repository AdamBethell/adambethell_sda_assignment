//
//  Presets.hpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 04/01/2018.
//
//

#ifndef Presets_hpp
#define Presets_hpp

#include "../JuceLibraryCode/JuceHeader.h"

/**
 Container class for static preset data
 */
class Presets
{
public:
    /**
     Get the names of the presets.
     @return A StringArray of the preset names.
     */
    static const StringArray& getNames() { return names; }
    /**
     Get the preset data for a given index.
     
     @see getNames
     @param index The index of the data. This index matches the index of the preset name.
     @return The preset data as a string 
     */
    static String getDataAtIndex (int index) { return data[index]; }
private:
    static const StringArray data;
    static const StringArray names;
};

#endif /* Presets_hpp */
