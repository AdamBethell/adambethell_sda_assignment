//
//  ProgrammerComponent.cpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 04/01/2018.
//
//

#include "ProgrammerComponent.hpp"

ProgrammerComponent::ProgrammerComponent (Array<PartialData>& partialArrayReference) :
programmer (partialArrayReference),
programmerWindow (this)
{
    addAndMakeVisible (&loadPresetButton);
    loadPresetButton.setButtonText ("Load preset");
    loadPresetButton.addListener (this);
    
    addAndMakeVisible (&openProgrammerButton);
    openProgrammerButton.setButtonText ("Open programmer");
    openProgrammerButton.addListener (this);
}
ProgrammerComponent::~ProgrammerComponent()
{
    
}

void ProgrammerComponent::paint (Graphics& g)
{
    
}
void ProgrammerComponent::resized()
{
    Rectangle<int> bounds = getLocalBounds();
    int halfSpace = bounds.getHeight() / 13;
    
    loadPresetButton.setBounds (bounds.removeFromTop (halfSpace * 2.0).reduced(1, 3));
    bounds.removeFromTop (halfSpace);
    openProgrammerButton.setBounds (bounds.removeFromTop (halfSpace * 2.0).reduced(1, 3));
}

void ProgrammerComponent::buttonClicked (Button* button)
{
    if (button == &loadPresetButton)
    {
        PopupMenu menu;
        const StringArray& options = Presets::getNames();
        for (int i = 0; i < options.size(); i++)
        {
            menu.addItem(i+1, options[i]);
        }
        int resultingIndexPlusOne = menu.showAt (&loadPresetButton);
        
        if (resultingIndexPlusOne == 0) return;
        
        bool result = programmer.loadProgram (Presets::getDataAtIndex (resultingIndexPlusOne-1));
        
        if (result)
        {
            if (listener != nullptr)
                listener->dataUpdated();
        }
        else
        {
            DBG ("Load Failed");
        }
    }
    else if (button == &openProgrammerButton)
    {
        programmerWindow.setVisible (false);
        programmerWindow.setVisible (true);
    }
}

bool ProgrammerComponent::loadFromScript (String script)
{
    bool result = programmer.loadProgram (script);
    
    if (result)
    {
        if (listener != nullptr)
            listener->dataUpdated();
    }
    else
    {
        DBG ("Load Failed");
    }
    
    return result;
}

