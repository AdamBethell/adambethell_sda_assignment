//
//  ProgrammerCodingComponent.hpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 05/01/2018.
//
//

#ifndef ProgrammerCodingComponent_hpp
#define ProgrammerCodingComponent_hpp

#include "../JuceLibraryCode/JuceHeader.h"

/**
 A code editor that overrides handleReturnKey to remove caridge return.
 */
class ProgrammerEditorComponent : public CodeEditorComponent
{
public:
    /**
     Constructor.
     */
    ProgrammerEditorComponent (CodeDocument& document) :
    CodeEditorComponent (document, nullptr)
    {}
    
    void handleReturnKey() override
    {
        insertTextAtCaret ("\n");
    }
};

/**
 The programming area for programmatically creating synths.
 */
class ProgrammerCodingComponent :   public Component,
                                    public Button::Listener
{
public:
    /**
     Used to receive callbacks when the user attempts to load a script written in the programmer.
     */
    class Listener
    {
    public:
        virtual ~Listener() {}
        /**
         Attepts to parse and load a script.
         */
        virtual bool loadFromScript (String script) = 0;
    };
    
    /**
     Constructor.
     */
    ProgrammerCodingComponent (Listener* _listener);
    /** 
     Destructor.
     */
    ~ProgrammerCodingComponent();
    
    void resized() override;
    
    void buttonClicked (Button* button) override;
private:
    Listener* listener;
    
    CodeDocument document;
    ProgrammerEditorComponent editor;
    TextButton loadButton;
};

/**
 The document window holding the programming area.
 */
class ProgrammerWindow : public DocumentWindow
{
public:
    /**
     Constructor
     */
    ProgrammerWindow (ProgrammerCodingComponent::Listener* listener) :
    DocumentWindow ("Programmer", Colours::darkgrey, closeButton, true)
    {
        RectanglePlacement placement (RectanglePlacement::xRight
                                      | RectanglePlacement::yBottom
                                      | RectanglePlacement::doNotResize);
        
        Rectangle<int> area (0, 0, 800, 400);
        Rectangle<int> result (placement.appliedTo (area, Desktop::getInstance().getDisplays()
                                                    .getMainDisplay().userArea.reduced (20)));
        setBounds (result);
        
        setResizable (false, false);
        setUsingNativeTitleBar (true);
        
        setContentOwned (new ProgrammerCodingComponent (listener), true);
    }
    /**
     Destructor
     */
    ~ProgrammerWindow(){}
    void closeButtonPressed() override
    {
        setVisible (false);
    }
private:
};

#endif /* ProgrammerCodingComponent_hpp */
