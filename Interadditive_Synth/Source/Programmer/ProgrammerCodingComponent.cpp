//
//  ProgrammerCodingComponent.cpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 05/01/2018.
//
//

#include "ProgrammerCodingComponent.hpp"

ProgrammerCodingComponent::ProgrammerCodingComponent (Listener* _listener) :
listener (_listener),
editor (document)
{
    setSize (500, 250);
    
    addAndMakeVisible (&editor);
    
    
    addAndMakeVisible (&loadButton);
    loadButton.setButtonText ("Load");
    loadButton.addListener (this);
}

ProgrammerCodingComponent::~ProgrammerCodingComponent()
{
    
}

void ProgrammerCodingComponent::resized()
{
    Rectangle<int> bounds = getLocalBounds();
    
    if (bounds.getWidth() < 100) return;
    
    loadButton.setBounds (bounds.removeFromBottom (50));
    editor.setBounds(bounds);
}

void ProgrammerCodingComponent::buttonClicked (Button* button)
{    
    bool result = listener->loadFromScript (document.getAllContent());
    
    if (!result)
    {
        // build failed
    }
}
