//
//  ProgrammerComponent.hpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 04/01/2018.
//
//

#ifndef ProgrammerComponent_hpp
#define ProgrammerComponent_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "../SimpleStructs.hpp"
#include "Presets.hpp"
#include "Programmer.hpp"
#include "ProgrammerCodingComponent.hpp"

/**
 The side bar handling presets and the programmer window.
 */
class ProgrammerComponent : public Component,
                            public Button::Listener,
                            public ProgrammerCodingComponent::Listener
{
public:
    /**
     Constructor.
     */
    ProgrammerComponent (Array<PartialData>& partialArrayReference);
    /**
     Destructor.
     */
    ~ProgrammerComponent();
    
    /* Component */
    void paint (Graphics& g) override;
    void resized() override;
    
    /* Button::Listener */
    void buttonClicked (Button* button) override;
    
    /* ProgrammerCodingComponent::Listener */
    bool loadFromScript (String script) override;
    
    /**
     Used to recieve callbacks when new data has been loaded.
     @see setListener
     */
    class Listener
    {
    public:
        virtual ~Listener() {}
        /**
         Called when new synth data has been loaded from presets or the programmer window.
         */
        virtual void dataUpdated() = 0;
    };
    /** 
     Sets the listener for a ProgrammerComponent
     */
    void setListener (Listener* _listener) { listener = _listener; }
private:
    Listener* listener;
    
    TextButton loadPresetButton;
    TextButton openProgrammerButton;
    
    Programmer programmer;
    ProgrammerWindow programmerWindow;
};

#endif /* ProgrammerComponent_hpp */
