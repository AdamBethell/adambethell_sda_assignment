//
//  Programmer.hpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 04/01/2018.
//
//

#ifndef Programmer_hpp
#define Programmer_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "../SimpleStructs.hpp"

/**
 This class parses scripts from the programming window or presets and loads the data.
 */
class Programmer
{
public:
    /**
     Constructor
     */
    Programmer (Array<PartialData>& partialArrayReference);
    /**
     Destructor
     */
    ~Programmer();
    
    /**
     Parses a script and loads the synth data
     
     @return Returns true or false based on if the script was succesfully parsed and loaded
     */
    bool loadProgram (String program);
    
    /**
     Enum used for parsing types of decliration in the programmer scripts
     */
    struct LineTypes
    {
        const static String AmpEnv;
        const static String PitchEnv;
        const static String Partials;
    };
private:
    void initLocalData();
    
    bool readEnvelopes (CodeDocument::Iterator& iterator);
    bool parseEnv (CodeDocument::Iterator& iterator, String type);
    bool setAmpEnvPointsToRelative (Envelope& env);
    bool setPitchEnvPointsToRelative (Envelope& env);
    
    bool readPartials (CodeDocument::Iterator& iterator);
    bool setEnvelope (CodeDocument::Iterator& iterator, Envelope** ampEnvToSet, Envelope** pitchEnvToSet);
    
    bool getXYValues (CodeDocument::Iterator& iterator, float* xValue, float* yValue);
    String parseDecimalStr (CodeDocument::Iterator& iterator);
    bool parseNumberOrFraction (CodeDocument::Iterator& iterator, float* valueToFill);
    bool parseFloat (CodeDocument::Iterator& iterator, float* valueToFill);
    
    Array<Envelope> amplitudeEnvelopes;
    StringArray ampEnvSymbols;
    Array<Envelope> pitchEnvelopes;
    StringArray pitchEnvSymbols;
    
    Array<PartialData>& data;
    Array<PartialData> newData;
};

#endif /* Programmer_hpp */
