//
//  Presets.cpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 04/01/2018.
//
//

#include "Presets.hpp"

const StringArray Presets::data = {
    "aenv0(50)[0@0][1@10][0.8@20][0.8@500][0@550]\npenv0[0@0]\npart<aenv0><penv0>[1@1]",
    "aenv0(50)[0@0][1@10][0.8@20][0.8@500][0@550]\npenv0[0@0]\npart<aenv0><penv0>[1@1][3@1/3][5@1/5][7@1/7][9@1/9]",
    "aenv0(50)[0@0][1@10][0.8@20][0.8@500][0@550]\npenv0[0@0]\npart<aenv0><penv0>[1@1][2@1/2][3@1/3][4@1/4][5@1/5][6@1/6][7@1/7][8@1/8][9@1/9]",
    "aenv0(50)[0@0][1@10][0.8@20][0.8@500][0@550]\npenv0[0@0]\npart<aenv0><penv0>[1@1][3@1/9][5@1/25][7@1/49][9@1/81]",
    "aenv0(500)[0@0][1@10][0.8@30]\npenv0[-12@0][7@50][0@100]\naenv1(50)[0@0][1@700]\npart<aenv0><penv0>[1@1][3@1/3][5@1/5][7@1/7]<aenv1>[2@1/2][4@1/4][6@1/6]"
};
const StringArray Presets::names = {
    "Sine",
    "Square",
    "Sawtooth",
    "Triangle",
    "Bouncy Square"
};
