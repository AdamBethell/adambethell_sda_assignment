//
//  Programmer.cpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 04/01/2018.
//
//

#include "Programmer.hpp"

const String Programmer::LineTypes::AmpEnv = "aenv";
const String Programmer::LineTypes::PitchEnv = "penv";
const String Programmer::LineTypes::Partials = "part";

Programmer::Programmer (Array<PartialData>& partialArrayReference) :
data (partialArrayReference)
{
    
}
Programmer::~Programmer()
{
    
}

bool Programmer::loadProgram (String program)
{
    program.removeCharacters ("\r");
    
    initLocalData();
    
    CodeDocument document;
    document.replaceAllContent (program);
    CodeDocument::Iterator iterator (document);
    
    DBG ("Reading Envelopes");
    bool result = readEnvelopes (iterator);
    if (!result) return false;
    
    DBG ("Reading Partials");
    result = readPartials (iterator);
    if (!result) return false;
    
    return true;
}

void Programmer::initLocalData()
{
    newData.clear();
    amplitudeEnvelopes.clear();
    ampEnvSymbols.clear();
    pitchEnvelopes.clear();
    pitchEnvSymbols.clear();
}

bool Programmer::readEnvelopes (CodeDocument::Iterator& iterator)
{
    while (true)
    {
        String type = "";
        for (int i = 0; i < 4; i++) type += iterator.nextChar();
        
        if (type == LineTypes::AmpEnv || type == LineTypes::PitchEnv)
        {
//            DBG ("Parsing envelope");
            bool result = parseEnv (iterator, type);
            if (!result) return false;
        }
        else if (type == LineTypes::Partials)
        {
//            DBG ("Partials line found");
            if (amplitudeEnvelopes.size() == 0 || pitchEnvelopes.size() == 0) return false;
            
            return true;
        }
        else
        {
//            DBG ("Type '" + type + "' does not exsist");
            return false;
        }
    }
}

bool Programmer::readPartials (CodeDocument::Iterator& iterator)
{
    Envelope* ampEnvToUse = nullptr;
    Envelope* pitchEnvToUse = nullptr;
    
    while (true)
    {
        iterator.skipWhitespace();
        juce_wchar nextChar = iterator.peekNextChar();
        while (nextChar == '\n')
        {
            iterator.skip();
            nextChar = iterator.peekNextChar();
        }
        
//        DBG ("Checking envelopes");
//        if (ampEnvToUse == nullptr) DBG ("Amp env is nullptr");
//        if (pitchEnvToUse == nullptr) DBG ("Pitch env is nullptr");
        
        if (nextChar == '<')
        {
            DBG ("Setting envelope");
            bool result = setEnvelope (iterator, &ampEnvToUse, &pitchEnvToUse);
            if (!result) return false;
        }
        else if (nextChar == '[')
        {
            DBG ("Setting partial");
//            DBG ("Checking envelopes");
            if (ampEnvToUse == nullptr || pitchEnvToUse == nullptr) return false;
            
            float amplitude = 0;
            float multiplyer = 0;
            
//            DBG ("Getting XY values");
            bool result = getXYValues (iterator, &amplitude, &multiplyer);
            if (!result) return false;
            
//            DBG ("Checking X and Y values are in range");
            if (amplitude < 0 || amplitude > 1) return false;
            if (multiplyer < 1 || multiplyer > 20) return false;
            
            PartialData newPartial;
            
            newPartial.setMult (multiplyer);
            newPartial.setAmp (amplitude);
            
            Envelope* envToSet = nullptr;
            
            envToSet = newPartial.getAmplitudeEnv();
            envToSet->numPointsValue = ampEnvToUse->getNumPoints();
            envToSet->param0Value = ampEnvToUse->param0Value;
            envToSet->timeValue = ampEnvToUse->timeValue;
            envToSet->points.clear();
            for (int i = 0; i < ampEnvToUse->points.size(); i++)
            {
                envToSet->points.add (ampEnvToUse->points[i]);
            }
            
            envToSet = newPartial.getFrequencyEnv();
            envToSet->numPointsValue = pitchEnvToUse->getNumPoints();
            envToSet->param0Value = pitchEnvToUse->param0Value;
            envToSet->timeValue = pitchEnvToUse->timeValue;
            envToSet->points.clear();
            for (int i = 0; i < pitchEnvToUse->points.size(); i++)
            {
                envToSet->points.add (pitchEnvToUse->points[i]);
            }
            
            newData.add (newPartial);
        }
        else if (iterator.isEOF())
        {
            DBG ("Assigning new data");
            data.clear();
            
            for (auto partialData : newData)
            {
                data.add (partialData);
            }
            
            return true;
        }
        else
        {
//            DBG ("Invalid char found");
            return false;
        }
    }
}

bool Programmer::setEnvelope (CodeDocument::Iterator& iterator, Envelope** ampEnvToSet, Envelope** pitchEnvToSet)
{
//    DBG ("Finding '<'");
    iterator.skipWhitespace();
    if (iterator.nextChar() != '<') return false;
    
    iterator.skipWhitespace();
    
    String type = "";
    for (int i = 0; i < 4; i++) type += iterator.nextChar();
    
    iterator.skipWhitespace();
    
//    DBG ("Getting EnvSymbol");
    String envSymbol = parseDecimalStr (iterator);
    if (envSymbol == String::empty) return false;
    
    envSymbol = type + envSymbol;
    
    DBG ("Env '" + envSymbol + "' found");
    
//    DBG ("Finding '>'");
    iterator.skipWhitespace();
    if (iterator.nextChar() != '>') return false;
    
    if (type == LineTypes::AmpEnv)
    {
//        DBG ("Finding amp env from array");
        int index = ampEnvSymbols.indexOf (envSymbol);
        if (index == -1) return false;
        
        *ampEnvToSet = &amplitudeEnvelopes.getReference (index);
    }
    else if (type == LineTypes::PitchEnv)
    {
//        DBG ("Finding pitch env from array");
        int index = pitchEnvSymbols.indexOf (envSymbol);
        if (index == -1) return false;
        
        *pitchEnvToSet = &pitchEnvelopes.getReference (index);
    }
    else
    {
//        DBG ("Type '" + type + "' does not exist");
        return false;
    }
    
    return true;
}

bool Programmer::parseEnv (CodeDocument::Iterator& iterator, String type)
{
//    DBG ("Getting envSymbol");
    String envSymbol = parseDecimalStr (iterator);
    if (envSymbol == String::empty) return false;
    
    envSymbol = type + envSymbol;
    Envelope envData;
    envData.points.clear();
    
//    DBG ("Symbol '" + envSymbol + "' found");
    
    float maxXTimeValue = 10;
    float maxYSTValue = 1;
    float releaseValue = 10;
    int numPoints = 0;
    
    bool isAmp = false;
    
    if (type == LineTypes::AmpEnv)
    {
        
        iterator.skipWhitespace();
        juce_wchar nextChar = iterator.nextChar();
        
//        DBG ("Getting release for ampEnv");
        
//        DBG ("Finding '('");
        if (nextChar != '(') return false;
        
        iterator.skipWhitespace();
        
//        DBG ("Parsing Number or Fraction");
        if (!parseNumberOrFraction (iterator, &releaseValue)) return false;
        
        iterator.skipWhitespace();
        nextChar = iterator.nextChar();
        
//        DBG ("Finding ')'");
        if (nextChar != ')') return false;
        
        isAmp = true;
    }
    
    while (true)
    {
//        DBG ("Num points: " + String (envData.points.size()));
        float xValue = 0;
        float yValue = 0;

//        DBG ("Getting XY values");
        bool result = getXYValues (iterator, &xValue, &yValue);
        if (!result) return false;
        
//        DBG ("XY: " + String(xValue) + ", " + String(yValue));
        envData.points.add (Point<float>(xValue, yValue));
        
        if (xValue > maxXTimeValue) maxXTimeValue = xValue;
        if (fabsf (yValue) > maxYSTValue) maxYSTValue = fabsf (yValue);
        
        numPoints++;
        
        
        if (iterator.peekNextChar() == '\n')
        {
//            DBG ("End of env found");
            iterator.skipToEndOfLine();
            
            envData.numPointsValue = numPoints;
            envData.timeValue = maxXTimeValue;
            
            if (isAmp)
            {
                envData.param0Value = releaseValue;
                
//                DBG ("Setting amp points to relative");
                bool result = setAmpEnvPointsToRelative (envData);
                if (!result) return false;
                
                amplitudeEnvelopes.add (envData);
                ampEnvSymbols.add (envSymbol);
            }
            else
            {
                envData.param0Value = maxYSTValue;
                
//                DBG ("Setting pitch points to relative");
//                DBG ("Num points: " + String (envData.points.size()));
                bool result = setPitchEnvPointsToRelative (envData);
                if (!result) return false;
                
                pitchEnvelopes.add (envData);
                pitchEnvSymbols.add (envSymbol);
            }
            
            return true;
        }
    }
    
}

bool Programmer::getXYValues (CodeDocument::Iterator& iterator, float* xValue, float* yValue)
{
    iterator.skipWhitespace();
    juce_wchar nextChar = iterator.nextChar();
    
//    DBG ("Finding '['");
    if (nextChar != '[') return false;
    
    iterator.skipWhitespace();
    
//    DBG ("Parsing Y value");
    if (!parseNumberOrFraction (iterator, yValue)) return false;
    
    iterator.skipWhitespace();
    
//    DBG ("Finding '@'");
    nextChar = iterator.nextChar();
    if (nextChar != '@') return false;
    
    iterator.skipWhitespace();
    
//    DBG ("Parsing X value");
    if (!parseNumberOrFraction (iterator, xValue)) return false;
    
    iterator.skipWhitespace();
    
//    DBG ("Finding ']'");
    nextChar = iterator.nextChar();
    if (nextChar != ']') return false;
    
    return true;
}

String Programmer::parseDecimalStr (CodeDocument::Iterator& iterator)
{
    String number = "";
    while (true)
    {
        const juce_wchar nextChar = iterator.peekNextChar();
        
        if (nextChar >= '0' && nextChar <= '9')
        {
            number += nextChar;
            iterator.skip();
        }
        else
        {
            if (number == "") return String::empty;
            
            return number;
        }
        
    }
}

bool Programmer::parseNumberOrFraction (CodeDocument::Iterator& iterator, float* valueToFill)
{
    float lhs = 0;
    
    bool result = parseFloat (iterator, &lhs);
    if (!result) return false;
    
    iterator.skipWhitespace();
    
//    DBG ("lhs = " + String(lhs));
    
    if (iterator.peekNextChar() == '/')
    {
        iterator.skip();
        iterator.skipWhitespace();
        float rhs = 0;
        
        bool result = parseFloat (iterator, &rhs);
        if (!result) return false;
        
//        DBG ("rhs = " + String(rhs));
        
        lhs = lhs / rhs;
    }
    
//    DBG ("value = " + String(lhs));
    *valueToFill = lhs;
    
    return true;
}

bool Programmer::parseFloat (CodeDocument::Iterator& iterator, float* valueToFill)
{
    String number = "";
    bool isNegative = false;
    
    if (iterator.peekNextChar() == '-')
    {
        isNegative = true;
        iterator.skip();
    }
        
    while (true)
    {
        const juce_wchar nextChar = iterator.peekNextChar();
        
        if ((nextChar >= '0' && nextChar <= '9') || nextChar == '.')
        {
            number += nextChar;
            iterator.skip();
        }
        else
        {
            if (number == "") return false;
            
            *valueToFill = number.getFloatValue();
            
            if (isNegative) *valueToFill = *valueToFill * -1.0;
            
            return true;
        }
        
    }
}
bool Programmer::setAmpEnvPointsToRelative (Envelope& env)
{
    for (int i = 0; i < env.points.size(); i++)
    {
        Point<float>& point = env.points.getReference (i);
        
        point.setX (point.getX() / env.timeValue);
        if (point.getX() < 0 || point.getX() > 1) return false;
        if (point.getY() < 0 || point.getY() > 1) return false;
    }
    
    return true;
}
bool Programmer::setPitchEnvPointsToRelative (Envelope& env)
{
//    DBG ("Num points: " + String (env.points.size()));
    for (int i = 0; i < env.points.size(); i++)
    {
        Point<float>& point = env.points.getReference (i);
        
//        DBG ("Before: " + String(point.getX()) + ", " + String(point.getY()));
        
        point.setX (point.getX() / env.timeValue);
        point.setY (((point.getY() / env.param0Value) / 2.0) + 0.5);
        
//        DBG ("After: " + String(point.getX()) + ", " + String(point.getY()));
        if (point.getX() < 0 || point.getX() > 1) return false;
        if (point.getY() < 0 || point.getY() > 1) return false;
    }
    
    return true;
}
