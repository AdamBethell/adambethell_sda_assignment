//
//  SimpleStructs.hpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 20/11/2017.
//
//

#ifndef SimpleStructs_hpp
#define SimpleStructs_hpp

#include "../JuceLibraryCode/JuceHeader.h"
/**
 An envelope.
 An envelope with an array of points and 3 parameter values.
 */
struct Envelope
{
public:
    /**
     Constructor
     */
    Envelope()
    {
        numPointsValue = 1;
        points.add (Point<float> (0.1, 0.5));
    }
    /**
     Destructor
     */
    ~Envelope(){}
    
    Array<Point<float>> points;
    
    /**
     Sets the number of points in the envelope.
     If the new number of points is less than the current, points will be removed and vis-versa.
     */
    void setNumPoints (int numPoints)
    {
        while (points.size() > numPoints)
        {
            points.removeLast();
        }
        
        while (points.size() < numPoints)
        {
            points.add (Point<float>(points.getLast().getX(), points.getLast().getY()));
        }
        numPointsValue = numPoints;
    }
    /**
     Gets the number of points in the envelope.
     */
    int getNumPoints() const { return numPointsValue; }
    
    int timeValue = 500;
    float param0Value = 10;
    int numPointsValue;
};

/**
 The data representing one partial.
 The data for one partial includes its amplitude, multiple of the fundimental, amplitude envelope, and pitch envelope.
 */
struct PartialData
{
public:
    /** 
     Constructor
     */
    PartialData() : id (++idGen) {}
    /**
     Constructor
     */
    PartialData (float multiplyer, float amplitude) : id (++idGen)
    {
        setMult (multiplyer);
        setAmp (amplitude);
    }
    /**
     Destructor
     */
    ~PartialData(){}
    
    /**
     Sets the multiple of the fundamental for this partial.
     Whole number values are harmonic partials. Non-whole number valyes are inharmonic partials.
     */
    void setMult (float multiplyer)
    {
        if (multiplyer < 1) multiplyer = 1;
        else if (multiplyer > 20) multiplyer = 20;
        
        mult = multiplyer;
    }
    /**
     Sets the amplitude for this partial.
     */
    void setAmp (float amplitude)
    {
        if (amplitude < 0) amplitude = 0;
        else if (amplitude > 1) amplitude = 1;
        
        amp = amplitude;
    }
    
    /**
     Gets the multiplyer value of this partial.
     */
    float getMult() const { return mult; }
    /**
     Gets the ampltide value of this partial.
     */
    float getAmp() const { return amp; }
    /**
     Gets the unique ID of this partial.
     This is used for tracking partials between the data and visual representations.
     */
    int getId() const { return id; }
    /**
     Gets a pointer to the pitch envelope of this partial.
     */
    Envelope* getFrequencyEnv() { return &freqEnv; }
    /**
     Gets a pointer to this amplitude envelope of this partial.
     */
    Envelope* getAmplitudeEnv() { return &ampEnv; }
private:
    static int idGen;

    const int id;
    float mult;
    float amp;
    Envelope ampEnv;
    Envelope freqEnv;
};
#endif /* SimpleStructs_hpp */
