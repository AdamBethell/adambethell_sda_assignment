//
//  DraggablePoint.cpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 03/01/2018.
//
//

#include "DraggablePoint.hpp"

DraggablePoint::DraggablePoint (PointColours _colours, String _dragAndDropDescription, Array<DraggablePoint*>& _allPoints) :
colours (_colours),
dragAndDropDescription (_dragAndDropDescription),
highlighted (true),
allPoints (_allPoints)
{
    allPoints = _allPoints;
    
    allPoints.add (this);
    
    innerColour = colours.innerColourWhenOff;
    outerColour = colours.outerColourWhenOff;
    
    unselectAll();
    highlight();
}

DraggablePoint::~DraggablePoint()
{
    allPoints.removeFirstMatchingValue (this);
}


// STATIC MEMBERS
void DraggablePoint::unselectAll()
{
    for (auto* ptr : allPoints)
    {
        ptr->unHighlight();
    }
}

// Functions
void DraggablePoint::paint (Graphics& g)
{
    g.setColour (outerColour);
    drawOuter (g);
    
    g.setColour (innerColour);
    drawInner (g);
}
void DraggablePoint::resized()
{
    
}
void DraggablePoint::mouseDown (const MouseEvent& event)
{
    unselectAll();
    highlight();
    
    mouseDownPosition = getMouseXYRelative();
    DragAndDropContainer::findParentDragContainerFor (this)->startDragging (dragAndDropDescription, this);
    
    repaint();
}

void DraggablePoint::mouseEnter (const MouseEvent& event)
{
    if (!highlighted)
    {
        outerColour = colours.outerColourWhenOver;
        innerColour = colours.innerColourWhenOver;
        
        repaint();
    }
}
void DraggablePoint::mouseExit (const MouseEvent& event)
{
    if (!highlighted)
    {
        innerColour = colours.innerColourWhenOff;
        outerColour = colours.outerColourWhenOff;
        
        repaint();
    }
}

void DraggablePoint::setSelected()
{
    unselectAll();
    highlight();
    repaint();
}

bool DraggablePoint::hasMoved()
{
    if (mouseDownPosition == getMouseXYRelative())
        return false;
    
    return true;
}

void DraggablePoint::highlight()
{
    highlighted = true;
    
    innerColour = colours.innerColourWhenHilighted;
    outerColour = colours.outerColourWhenHilighted;
    
    repaint();
}
void DraggablePoint::unHighlight()
{
    highlighted = false;
    
    innerColour = colours.innerColourWhenOff;
    outerColour = colours.outerColourWhenOff;
    
    repaint();
}
