//
//  SineOsc.cpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 02/01/2018.
//
//

#include "SineOsc.hpp"



SineOsc::SineOsc (float _frequency, float _outputAmplitude, Envelope _freqEnv, Envelope _ampEnv) :
fReadPos (0),
releaseSamplesCount (0),
ampReadPos (0),
lastAmplitudeFromEnv (0.0),
freqReadPos (0),
inRelease (false),
releaseFinished (false),
outputAmplitude (_outputAmplitude),
frequency (_frequency),
freqEnv (_freqEnv),
ampEnv (_ampEnv)
{
    for (int i = 0; i < 48000; i++)
    {
        sineWave[i] = sin ((i / 47999.0) * M_PI * 2.0);        
    }
    
    setAmplitudeWavetable();
    setFrequencyWavetable();
}

SineOsc::~SineOsc()
{
    
}

double SineOsc::tick()
{
    double outputSample = 0.0;
    
    outputSample = getInterpolatedSample() * getAmplitudeFromEnv();
    
    float freq = getFrequencyFromEnv();
    fReadPos += freq;
    
    while (fReadPos >= 48000) fReadPos -= 48000;
    
    return outputSample * outputAmplitude;
}

double SineOsc::getInterpolatedSample()
{
    int iReadPos = (int)fReadPos;
    float frac = fReadPos - iReadPos;
    
    float v[4] = {0};
    int tempIndex;
    
    tempIndex = iReadPos - 1;
    if (tempIndex == -1)
        tempIndex = 47999;
    v[0] = sineWave[tempIndex];
    
    v[1] = sineWave[iReadPos];
    
    tempIndex = iReadPos + 1;
    if (tempIndex == 48000)
        tempIndex = 0;
    v[2] = sineWave[tempIndex];
    
    tempIndex = iReadPos + 2;
    if (tempIndex >= 48000)
        tempIndex -= 48000;
    v[3] = sineWave[tempIndex];
    

    float a[4] = {0};
    
    a[0] = v[1];
    a[1] = 0.5 * (v[2] - v[0]);
    a[2] = v[0] - 2.5 * v[1] + 2 * v[2] - 0.5 * v[3];
    a[3] = 0.5 * (v[3] - v[0]) + 1.5 * (v[1] - v[2]);
    
    return a[3] * powf (frac, 3) + a[2] * powf (frac, 2) + a[1] * frac + a[0];

}
void SineOsc::setAmplitudeWavetable()
{
    for (auto& point : ampEnv.points)
    {
        point.setY (point.getY() * point.getY() * point.getY());
        point.setX (point.getX() * (ampEnv.timeValue / 1000.0) * 48000);
    }
    
    numReleaseSamples = (ampEnv.param0Value / 1000.0) * 48000;
    
    ampEnvNumSamples = (ampEnv.timeValue / 1000.0) * 48000;
    ampWavetable = new float[ampEnvNumSamples];
    
    float prevAmp = 0.0;
    for (int i = 0; i < ampEnvNumSamples; i++)
    {
        Point<float> lowerPoint (0, 0);
        Point<float> upperPoint (0, 0);
        float amplitude = 0.0;
        
        int j = 0;
        for (j = 0; j < ampEnv.points.size(); j++)
        {
            if (i < ampEnv.points[j].getX())
            {
                upperPoint = ampEnv.points[j];
                
                if (j != 0) lowerPoint = ampEnv.points[j-1];
                
                break;
                
                
            }
        }
        
        if (j == ampEnv.points.size())
        {
            ampWavetable[i] = prevAmp;
        }
        else
        {
            float frac = (i - lowerPoint.getX()) / (upperPoint.getX() - lowerPoint.getX());
            float yDiff = upperPoint.getY() - lowerPoint.getY();
            amplitude = frac * yDiff + lowerPoint.getY();
            
            prevAmp = amplitude;
            ampWavetable[i] = amplitude;
        }
        
    }
}
float SineOsc::getAmplitudeFromEnv()
{
    float outputAmp = 0.0;
    
    if (!inRelease)
    {
        if (ampReadPos >= ampEnvNumSamples)
        {
            outputAmp = ampWavetable[ampEnvNumSamples-1];
        }
        else
        {
            outputAmp = ampWavetable[ampReadPos];
        }
        
        ampReadPos++;
        
        lastAmplitudeFromEnv = outputAmp;
        return outputAmp;
    }
    else
    {
        outputAmp = lastAmplitudeFromEnv * (1.0 - ((float)releaseSamplesCount / (float)numReleaseSamples));
        if (outputAmp < 0.0) outputAmp = 0.0;
        
        releaseSamplesCount++;
        if (releaseSamplesCount >= numReleaseSamples) releaseFinished = true;
        
        return outputAmp;
    }
}

void SineOsc::setFrequencyWavetable()
{
    for (auto& point : freqEnv.points)
    {
        point.setY ((point.getY() * 2.0 - 1.0) * freqEnv.param0Value);
        point.setX (point.getX() * (freqEnv.timeValue / 1000.0) * 48000);
    }
    
    freqEnvNumSamples = (freqEnv.timeValue / 1000.0) * 48000;
    freqWavetable = new float[freqEnvNumSamples];
    
    float prevFrequency = frequency;
    for (int i = 0; i < freqEnvNumSamples; i++)
    {
        Point<float> lowerPoint (0, 0);
        Point<float> upperPoint (0, 0);
        
        int j = 0;
        for (j = 0; j < freqEnv.points.size(); j++)
        {
            if (i < freqEnv.points[j].getX())
            {
                upperPoint = freqEnv.points[j];
                
                if (j != 0) lowerPoint = freqEnv.points[j-1];
                
                break;
            }
        }
        
        if (j == freqEnv.points.size())
        {
            freqWavetable[i] = prevFrequency;
        }
        else
        {
            float frac = (i - lowerPoint.getX()) / (upperPoint.getX() - lowerPoint.getX());
            float yDiff = upperPoint.getY() - lowerPoint.getY();
            float semitones = frac * yDiff + lowerPoint.getY();
            
            prevFrequency = frequency * powf(2.0, (semitones / 12.0));
            freqWavetable[i] = prevFrequency;
            
        }
        
        
        
    }

}

float SineOsc::getFrequencyFromEnv()
{
    float outputFreq = 0.0;
    
    if (freqReadPos >= freqEnvNumSamples)
    {
        outputFreq = freqWavetable[freqEnvNumSamples-1];
    }
    else
    {
        outputFreq = freqWavetable[freqReadPos];
    }
    
    freqReadPos++;
    
    return outputFreq;
}
