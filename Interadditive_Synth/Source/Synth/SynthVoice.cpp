//
//  SynthVoice.cpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 02/01/2018.
//
//

#include "SynthVoice.hpp"

SynthVoice::SynthVoice (int _midiNote, Array<PartialData> partialData) :
inRelease (false),
midiNote (_midiNote),
noteFreqency (mtof (midiNote)),
finished (false)
{
//    DBG ("Voice created with " + String(partialData.size()) + " partials");
    for (int i = 0; i < partialData.size(); i++)
    {
        SineOsc* newOsc = new SineOsc (
                                       noteFreqency * partialData[i].getMult(),
                                       partialData[i].getAmp(),
                                       *partialData[i].getFrequencyEnv(),
                                       *partialData[i].getAmplitudeEnv()
                                       );
//        DBG (String(noteFreqency * partialData[i].getMult()) + "Hz @" + String(partialData[i].getAmp()));
        partials.add (newOsc);
    }
}

SynthVoice::~SynthVoice()
{
    
}

float SynthVoice::mtof (int noteNumber)
{
    /* Turn midi note number into a frequency */
    return (440.0 * pow (2.0, (noteNumber-69.0) / 12.0));
}

void SynthVoice::release()
{
    for (auto* p : partials)
    {
        p->release();
    }
    inRelease = true;
    
}

double SynthVoice::tick()
{
    if (inRelease)
    {
        bool releaseFinished = true;
        
        for (auto* p : partials)
        {
            if (!p->isReleaseFinished()) releaseFinished = false;
        }
        
        if (releaseFinished) listener->postMessage (new ReleaseFinishedMessage (this));
    }
    
    double sample = 0.0;
    
    for (auto* p : partials)
    {
        sample += p->tick();
    }
    
    return sample;
}
