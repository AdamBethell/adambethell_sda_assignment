//
//  AdditiveSynth.cpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 02/01/2018.
//
//

#include "AdditiveSynth.hpp"

AdditiveSynth::AdditiveSynth (Array<PartialData>& partialData) : partialDataReference (partialData)
{
    
}
AdditiveSynth::~AdditiveSynth()
{
    
}

void AdditiveSynth::noteOn (int midiNote)
{
    noteOff (midiNote);
    
    // Get lock for partialData
    SynthVoice* newVoice = new SynthVoice (midiNote, partialDataReference);
    newVoice->setMessageListener (this);
    
    const ScopedLock lock (voicesArrayLock);
    voices.add (newVoice);
   
}
void AdditiveSynth::noteOff (int midiNote)
{
    for (auto* v : voices)
    {
        if (v->getNote() == midiNote)
        {
            v->release();
        }
    }
}

double AdditiveSynth::tick()
{
    double sample = 0.0;
    
    const ScopedLock lock (voicesArrayLock);
    for (auto* v : voices)
    {
        if (! v->isFinished())
        {
            sample += v->tick();
        }
    }
    
    return sample;
}

void AdditiveSynth::handleMessage (const Message& message)
{
    const SynthVoice::ReleaseFinishedMessage* rfm = dynamic_cast<const SynthVoice::ReleaseFinishedMessage*> (&message);
    if (rfm != nullptr)
    {
        const ScopedLock lock (voicesArrayLock);
        voices.removeObject (rfm->synthVoice);
    }
}
