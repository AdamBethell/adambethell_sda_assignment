//
//  SynthVoice.hpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 02/01/2018.
//
//

#ifndef SynthVoice_hpp
#define SynthVoice_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "../SimpleStructs.hpp"
#include "SineOsc.hpp"

/**
 A single voice in the additive synth.
 @see AdditiveSynth, SineOsc
 */
class SynthVoice
{
public:
    /**
     Constructor.
     */
    SynthVoice (int _midiNote, Array<PartialData> partialData);
    /**
     Destructor.
     */
    ~SynthVoice();
    
    /**
     Gets the midi note this voice is playing.
     */
    int getNote() const { return midiNote; }
    /**
     Returns true of false based on if the voice has finished its release.
     */
    bool isFinished() const { return finished; };
    
    /**
     Sets the voice to start its release.
     */
    void release();
    
    /**
     Gets the next sample from this voice.
     */
    double tick();
    
    /**
     Sets the message listener.
     This will be the additive synth. Notifies when this voice has finished its release so it can be deleted.
     @see AdditiveSynth
     */
    void setMessageListener (MessageListener* _listener) { listener = _listener; }
    
    /**
     A message that holds a pointer to this instance of SynthVoice.
     @see Message
     */
    struct ReleaseFinishedMessage : public Message
    {
        ReleaseFinishedMessage (SynthVoice* _synthVoice) : synthVoice (_synthVoice) {}
        SynthVoice* synthVoice;
    };
private:
    MessageListener* listener;
    
    float mtof (int noteNumber);
    
    bool inRelease;
    
    int midiNote;
    float noteFreqency;
    bool finished;
    OwnedArray<SineOsc> partials;
};

#endif /* SynthVoice_hpp */
