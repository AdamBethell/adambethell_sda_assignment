//
//  AdditiveSynth.hpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 02/01/2018.
//
//

#ifndef AdditiveSynth_hpp
#define AdditiveSynth_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "../SimpleStructs.hpp"
#include "SynthVoice.hpp"

/**
 An additive synth that configures itself based on the partial data created by the user through the UI.
 @see SynthVoice, SineOsc
 */
class AdditiveSynth : public MessageListener
{
public:
    /**
     Constructor.
     */
    AdditiveSynth (Array<PartialData>& partialData);
    /**
     Destructor.
     */
    ~AdditiveSynth();
    
    /**
     Makes a new voice for the note.
     */
    void noteOn (int midiNote);
    /**
     Sets the notes voice to release.
     */
    void noteOff (int midiNote);
    
    /**
     Gets the next sample from the synth.
     */
    double tick();
    
    void handleMessage (const Message& message) override;
    
private:
    CriticalSection voicesArrayLock;
    
    Array<PartialData>& partialDataReference;
    OwnedArray<SynthVoice> voices;
};

#endif /* AdditiveSynth_hpp */
