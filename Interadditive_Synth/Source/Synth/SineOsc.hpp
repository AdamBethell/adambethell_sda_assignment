//
//  SineOsc.hpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 02/01/2018.
//
//

#ifndef SineOsc_hpp
#define SineOsc_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "../SimpleStructs.hpp"

/**
 A single partial of a SynthVoice.
 @see SynthVoice, AdditiveSynth
 */
class SineOsc
{
public:
    /**
     Constructor.
     Uses the frequency and envelope data to contruct wavetables for the sine wave, aplitude envelope, and pitch envelope.
     */
    SineOsc (float _frequency, float _outputAmplitude, Envelope _freqEnv, Envelope _ampEnv);
    /**
     Destructor.
     */
    ~SineOsc();
    
    /**
     Gets the next sample from this partial.
     */
    double tick();
    
    /**
     Sets this partial to start its release.
     */
    void release() { inRelease = true; }
    
    /**
     Returns true of false based on if the voice has finished its release.
     */
    bool isReleaseFinished() const { return releaseFinished; }
private:
    double sineWave[48000];
    
    ScopedPointer<float> freqWavetable;
    ScopedPointer<float> ampWavetable;
    
    float fReadPos;
    
    int numReleaseSamples;
    int releaseSamplesCount;
    int ampEnvNumSamples;
    int ampReadPos;
    float lastAmplitudeFromEnv;
    
    int freqEnvNumSamples;
    int freqReadPos;
    
    bool inRelease;
    bool releaseFinished;
    
    
    
    double getInterpolatedSample();
    
    void setAmplitudeWavetable();
    float getAmplitudeFromEnv();
    
    void setFrequencyWavetable();
    float getFrequencyFromEnv();
    
    float outputAmplitude;
    float frequency;
    Envelope freqEnv;
    Envelope ampEnv;
};

#endif /* SineOsc_hpp */
