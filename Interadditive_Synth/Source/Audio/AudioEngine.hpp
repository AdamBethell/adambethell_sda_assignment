//
//  AudioEngine.hpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 03/01/2018.
//
//

#ifndef AudioEngine_hpp
#define AudioEngine_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "../Synth/AdditiveSynth.hpp"

/**
 The audio engine.
 Periodically scans for and adds new midi input divices. Passes midi note data onto the synth; audio data to the output.
 */
class AudioEngine : public AudioIODeviceCallback, public MidiInputCallback, public Timer
{
public:
    AudioEngine (Array<PartialData>& partialData);
    ~AudioEngine();
    
    /* AudioIODeviceCallback */
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    void audioDeviceStopped() override;
    
    /* MidiInputCallback */
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    
    /* Timer */
    void timerCallback() override;
    
private:
    AudioDeviceManager audioDeviceManager;
    
    AdditiveSynth additiveSynth;
    
    StringArray currentInputs;
};

#endif /* AudioEngine_hpp */
