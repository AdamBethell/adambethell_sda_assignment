//
//  AudioEngine.cpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 03/01/2018.
//
//

#include "AudioEngine.hpp"

AudioEngine::AudioEngine (Array<PartialData>& partialData) : additiveSynth (partialData)
{
    audioDeviceManager.initialiseWithDefaultDevices (0, 2);
    audioDeviceManager.addAudioCallback (this);
    
    currentInputs = MidiInput::getDevices();
    for (String s : currentInputs)
    {
        audioDeviceManager.setMidiInputEnabled(s, true);
    }
    audioDeviceManager.addMidiInputCallback(String::empty, this);
    
    startTimer (1000);
    
}
AudioEngine::~AudioEngine()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}

void AudioEngine::audioDeviceIOCallback (   const float** inputChannelData,
                                            int numInputChannels,
                                            float** outputChannelData,
                                            int numOutputChannels,
                                            int numSamples)
{
    for (int sample = 0; sample < numSamples; sample++)
    {
        float outputSample = additiveSynth.tick();

        for (int channel = 0; channel < numOutputChannels; channel++)
        {
            outputChannelData[channel][sample] = outputSample;
        }
    }
    
    
}
void AudioEngine::audioDeviceAboutToStart (AudioIODevice* device)
{
    
}
void AudioEngine::audioDeviceStopped()
{
    
}

void AudioEngine::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    if (message.isNoteOn())
    {
        additiveSynth.noteOn (message.getNoteNumber());
    }
    else if (message.isNoteOff())
    {
        additiveSynth.noteOff (message.getNoteNumber());
    }
}

void AudioEngine::timerCallback()
{
    StringArray updatedInputs =  MidiInput::getDevices();
    
    if (currentInputs.size() != 0)
    {
        for (int i = currentInputs.size()-1; i >= 0; i--)
        {
            if (!updatedInputs.contains(currentInputs[i]))
            {
                DBG("Input: " << currentInputs[i] << " lost");
                audioDeviceManager.setMidiInputEnabled(currentInputs[i], false);
                currentInputs.remove(i);
            }
        }
    }
    
    if (updatedInputs.size() != 0)
    {
        for (int i = 0; i < updatedInputs.size(); i++)
        {
            if (!currentInputs.contains(updatedInputs[i]))
            {
                DBG("Input: " << updatedInputs[i] << " found");
                audioDeviceManager.setMidiInputEnabled(updatedInputs[i], true);
                currentInputs.add(updatedInputs[i]);
            }
        }
    }
}
