//
//  EnvelopeComponent.cpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 04/12/2017.
//
//

#include "EnvelopeComponent.hpp"

EnvelopeComponent::EnvelopeComponent (Array<PartialData>& partialArrayReference) :
partialArray (partialArrayReference),
env (nullptr)
{
    addAndMakeVisible (&windowName);
    windowName.setColour (Label::ColourIds::textColourId, Colours::black);
    windowName.setJustificationType (Justification::topLeft);
    windowName.setInterceptsMouseClicks (false, false);
    
    addAndMakeVisible (&copyButton);
    copyButton.setButtonText ("Copy Env.");
    copyButton.addListener (this);
    addAndMakeVisible (&pasteButton);
    pasteButton.setButtonText ("Paste Env.");
    pasteButton.addListener (this);
    
    addAndMakeVisible (&timeLabel);
    timeLabel.setText ("Time", dontSendNotification);
    timeLabel.setColour (Label::ColourIds::textColourId, Colours::black);
    timeLabel.attachToComponent (&time, true);
    timeLabel.setJustificationType (Justification::centredRight);
    addAndMakeVisible (time);
    time.setRange (1.0, 10000.0, 1);
    time.setTextValueSuffix ("ms");
    time.setColour (Slider::ColourIds::textBoxTextColourId, Colours::black);
    time.setSliderStyle (Slider::SliderStyle::IncDecButtons);
    time.addListener (this);
    
    addAndMakeVisible (&param0Label);
    param0Label.setText ("Param0", dontSendNotification);
    param0Label.setColour (Label::ColourIds::textColourId, Colours::black);
    param0Label.setJustificationType (Justification::centredRight);
    param0Label.attachToComponent (&param0, true);
    addAndMakeVisible (param0);
    param0.setSliderStyle (Slider::SliderStyle::IncDecButtons);
    param0.setColour (Slider::ColourIds::textBoxTextColourId, Colours::black);
    param0.addListener (this);
    
    
    numPointsLabel.setText ("Points", dontSendNotification);
    numPointsLabel.setColour (Label::ColourIds::textColourId, Colours::black);
    numPointsLabel.setJustificationType (Justification::centredRight);
    addAndMakeVisible (&numPointsLabel);
    numPointsLabel.attachToComponent (&numPoints, true);
    addAndMakeVisible (&numPoints);
    numPoints.setSliderStyle (Slider::SliderStyle::IncDecButtons);
    numPoints.setColour (Slider::ColourIds::textBoxTextColourId, Colours::black);
    numPoints.setRange (1.0, 10.0, 1.0);
    numPoints.addListener (this);
    
    addAndMakeVisible (&envelopeView);
}
EnvelopeComponent::~EnvelopeComponent()
{
    
}

void EnvelopeComponent::paint (Graphics& g)
{
    g.fillAll (Colour (0xFFCDECFC));
}
void EnvelopeComponent::resized()
{
    Rectangle<int> bounds = getLocalBounds();
    Rectangle<int> sliderBounds = bounds.removeFromRight (85).reduced (1);
    
    param0.setBounds (sliderBounds.removeFromTop (sliderBounds.getHeight() / 3.0));
    numPoints.setBounds (sliderBounds.removeFromTop (sliderBounds.getHeight() / 2.0));
    time.setBounds (sliderBounds);
    
    Rectangle<int> topBar = bounds.removeFromTop (18);
    windowName.setBounds (topBar.removeFromLeft (200).reduced (1, 1));
    copyButton.setBounds (topBar.removeFromLeft (75));
    pasteButton.setBounds (topBar.removeFromLeft (75));
    
    Array<int> widths;
    widths.add (param0Label.getWidth());
    widths.add (numPointsLabel.getWidth());
    widths.add (timeLabel.getWidth());
    widths.sort();
    bounds.removeFromRight (widths.getLast());
    
    envelopeView.setBounds (bounds.reduced (2));
    
}

void EnvelopeComponent::newPartialSelected (int partialId)
{
//    DBG("size()" + String (partialArray.size()));
    
    selectedId = partialId;
    DBG("Selected ID: " + String (selectedId));
//    DBG("Stored value: " + String (env->numPointsValue));
    
    
    time.setValue (env->timeValue, dontSendNotification);
    numPoints.setValue (env->getNumPoints(), dontSendNotification);
    param0.setValue (env->param0Value, dontSendNotification);
    envelopeView.updateNodes();
}

void EnvelopeComponent::sliderValueChanged (Slider* slider)
{
    if (env == nullptr) return;
    
    if (slider == &time)
    {
        env->timeValue = (int)slider->getValue();
    }
    else if (slider == &numPoints)
    {
        env->setNumPoints ((int)slider->getValue());
    }
    else if (slider == &param0)
    {
        env->param0Value = slider->getValue();
    }
    
    envelopeView.updateNodes();
}

void EnvelopeComponent::buttonClicked (Button* button)
{
    if (button == &copyButton)
    {
        cachedEnv = *env;
    }
    else if (button == &pasteButton)
    {
        *env = cachedEnv;
        envelopeView.updateNodes();
    }
}
