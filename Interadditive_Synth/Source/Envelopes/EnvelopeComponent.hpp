//
//  EnvelopeComponent.hpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 04/12/2017.
//
//

#ifndef EnvelopeComponent_hpp
#define EnvelopeComponent_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "EnvelopeViewComponent.hpp"
/**
 The visual component for displaying envelope data.
 */
class EnvelopeComponent :   public Component,
                            public Slider::Listener,
                            public Button::Listener
{
public:
    /**
     Constructor.
     */
    EnvelopeComponent (Array<PartialData>& partialArrayReference);
    /**
     Destructor
     */
    ~EnvelopeComponent();
    
    void paint (Graphics& g) override;
    void resized() override;
    
    /**
     Called when a new partial has been selected in the PartialsComponent.
     @see PartialsDrawerComponent
     */
    virtual void newPartialSelected (int partialId);
    
    void sliderValueChanged (Slider* slider) override;
    
    void buttonClicked (Button* button) override;
    
protected:
    Label windowName;
    Label param0Label;
    Slider param0;
    
    Array<PartialData>& partialArray;
    EnvelopeViewComponent envelopeView;
    
    int selectedId;
    Envelope* env;
    
private:
    Label timeLabel;
    Slider time;
    Label numPointsLabel;
    Slider numPoints;
    
    TextButton copyButton;
    TextButton pasteButton;
    
    Envelope cachedEnv;
    
};

#endif /* EnvelopeComponent_hpp */
