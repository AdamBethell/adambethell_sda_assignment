//
//  DraggableNode.cpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 03/01/2018.
//
//

#include "DraggableNode.hpp"

Array<DraggablePoint*> DraggableNode::allNodes = Array<DraggablePoint*>();

DraggableNode::DraggableNode() :
DraggablePoint (DraggablePoint::PointColours (Colour (0xFFEE807F),
                                              Colour (0xFFD67BE8),
                                              Colour (0xFF008FFF),
                                              Colour (0xFF56F7CB),
                                              Colour (0xFF877FEE),
                                              Colour (0xFF56AFF7)),
                "Node",
                allNodes)
{
    
}
DraggableNode::~DraggableNode()
{
    
}

void DraggableNode::drawInner (Graphics& g)
{
    g.fillRect (getLocalBounds().toFloat().reduced (5));
}

void DraggableNode::drawOuter (Graphics& g)
{
    g.drawRect (getLocalBounds().toFloat().reduced (2), 3);
}
