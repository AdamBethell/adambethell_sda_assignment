//
//  FrequencyEnvComponent.hpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 04/12/2017.
//
//

#ifndef FrequencyEnvComponent_hpp
#define FrequencyEnvComponent_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "../EnvelopeComponent.hpp"
/**
 An specialised EnvelopeComponent.
 The FrequencyEnvComponent draws between nodes starting from the middle left. It also sets a "semitones" slider.
 @see EnvelopeComponent
 */
class FrequencyEnvComponent : public EnvelopeComponent
{
public:
    /**
     Constructor.
     */
    FrequencyEnvComponent (Array<PartialData>& partialArrayReference);
    /**
     Destructor.
     */
    ~FrequencyEnvComponent();
    
    /* EnvelopeComponent */
    void newPartialSelected (int partialId) override;
private:
};

#endif /* FrequencyEnvComponent_hpp */
