//
//  FrequencyEnvComponent.cpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 04/12/2017.
//
//

#include "FrequencyEnvComponent.hpp"

FrequencyEnvComponent::FrequencyEnvComponent (Array<PartialData>& partialArrayReference) : EnvelopeComponent (partialArrayReference)
{
    windowName.setText ("Frequency Envelope", dontSendNotification);
    
    param0Label.setText ("Range", dontSendNotification);
    param0.setRange (1.0, 12.0, 0.1);
    param0.setTextValueSuffix ("st");
    
    envelopeView.startPoint = Point<float> (0, 0.5);
}

FrequencyEnvComponent::~FrequencyEnvComponent()
{
}

void FrequencyEnvComponent::newPartialSelected (int partialId)
{
    for (int i = 0; i < partialArray.size(); i++)
    {
        if (partialArray.getReference (i).getId() == partialId)
        {
            env = partialArray.getReference (i).getFrequencyEnv();
            envelopeView.newEnvelopeSelected (env);
            break;
        }
    }
    EnvelopeComponent::newPartialSelected (partialId);
}
