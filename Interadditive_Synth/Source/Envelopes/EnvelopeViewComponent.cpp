//
//  EnvelopeViewComponent.cpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 04/12/2017.
//
//

#include "EnvelopeViewComponent.hpp"

EnvelopeViewComponent::EnvelopeViewComponent() : envelopeToDisplay (nullptr)
{
    
}
EnvelopeViewComponent::~EnvelopeViewComponent()
{
    
}

void EnvelopeViewComponent::paint (Graphics& g)
{
    g.setColour (Colours::black);
    g.drawRect (getLocalBounds(), 2);
    
    if (envelopeToDisplay == nullptr) return;
    
    Point<float> lastPoint = startPoint;
    for (Point<float> point : envelopeToDisplay->points)
    {
        g.drawLine (lastPoint.getX() * getWidth(),  (1.0 - lastPoint.getY()) * getHeight(),
                    point.getX() * getWidth(),      (1.0 - point.getY()) * getHeight(),
                    2);
        lastPoint = point;
    }
    
    g.drawLine (lastPoint.getX() * getWidth(),  (1.0 - lastPoint.getY()) * getHeight(),
                getWidth(),                     (1.0 - lastPoint.getY()) * getHeight(),
                2);
}
void EnvelopeViewComponent::resized()
{
    
}

bool EnvelopeViewComponent::isInterestedInDragSource (const SourceDetails& dragSourceDetails)
{
    if (dragSourceDetails.description == "Node") return true;
    
    return false;
}
void EnvelopeViewComponent::dragOperationStarted (const SourceDetails& dragSourceDetails)
{
    
}
void EnvelopeViewComponent::itemDropped (const SourceDetails& dragSourceDetails)
{
    DraggableNode* component = dynamic_cast<DraggableNode*> (dragSourceDetails.sourceComponent.get());
    if (component->hasMoved())
    {
        Point<int> point;
        point.setY (dragSourceDetails.localPosition.getY());
        point.setX (dragSourceDetails.localPosition.getX());
        DBG ("x: " + String((float)point.getX() / (float)getWidth()) +
             "\ty: " + String(1.0 - ((float)point.getY() / (float)getHeight())));
        
        component->setCentrePosition (point);
        
        int currentIndex = component->currentIndex;
        envelopeToDisplay->points.getReference (currentIndex).setXY ((float)point.getX() / (float)getWidth(),
                                                                     1.0 - ((float)point.getY() / (float)getHeight()));
        
        
        updateNodes();
        repaint();
    }
}


void EnvelopeViewComponent::newEnvelopeSelected (Envelope* env)
{
    envelopeToDisplay = env;
    updateNodes();
}

void EnvelopeViewComponent::updateNodes()
{
    if (envelopeToDisplay == nullptr) return;
 
    orderPointsToMatchNodes();
    nodeVisuals.clear();
    
    int count = 0;
    for (auto point : envelopeToDisplay->points)
    {
        DraggableNode* newNode = new DraggableNode();
        newNode->currentIndex = count++;
        
        Point<int> position;
        position.setX (point.getX() * (float)getWidth());
        position.setY ((1.0 - point.getY()) * (float)getHeight());
        
        newNode->setSize (20, 20);
        newNode->setCentrePosition (position);
        
        nodeVisuals.add (newNode);
        
        addAndMakeVisible (newNode);
    }
    
    repaint();
}

void EnvelopeViewComponent::orderPointsToMatchNodes()
{
    
    Array<Point<float>> orderedPoints;
    
    for (int i = 0; i < envelopeToDisplay->points.size(); i++)
    {
        // Value are between 0 and 1 so 2 will be higher than everything
        float smallest = 2;
        // -1 is a null value I can check for
        int indexOfSmallest = -1;
        
        for (int j = 0; j < envelopeToDisplay->points.size(); j++)
        {
            if (envelopeToDisplay->points[j].getX() < smallest)
            {
                smallest = envelopeToDisplay->points[j].getX();
                indexOfSmallest = j;
            }
        }
        
        orderedPoints.add(envelopeToDisplay->points[indexOfSmallest]);
        envelopeToDisplay->points.getReference (indexOfSmallest).setX (2);
    }
    
    envelopeToDisplay->points = orderedPoints;
    
    repaint();
}
