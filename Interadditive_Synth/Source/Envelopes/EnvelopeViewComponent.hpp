//
//  EnvelopeViewComponent.hpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 04/12/2017.
//
//

#ifndef EnvelopeViewComponent_hpp
#define EnvelopeViewComponent_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "../SimpleStructs.hpp"
#include "DraggableNode.hpp"

/**
 
 */
class EnvelopeViewComponent :   public Component,
                                public DragAndDropContainer,
                                public DragAndDropTarget
{
public:
    /**
     Constructor
     */
    EnvelopeViewComponent();
    /**
     Destructor
     */
    ~EnvelopeViewComponent();
    
    /* Component */
    void paint (Graphics& g) override;
    void resized() override;
    
    /* DragAndDropTarget */
    bool isInterestedInDragSource (const SourceDetails& dragSourceDetails) override;
    void itemDropped (const SourceDetails& dragSourceDetails) override;
    void dragOperationStarted (const SourceDetails& dragSourceDetails) override;
    
    /* Members */
    /**
     Updates the evelope visuals with the data pointed to by the argument.
     This is often called when a new partial is selected.
     */
    void newEnvelopeSelected (Envelope* env);
    
    /**
     Updates the evelope visuals with the the current data.
     This is often called with the partial has not changed but the data has been affected.
     */
    void updateNodes();
    /**
     Orders the data points to match the visual points.
     This is used to ensure the indexes of the visual and data representations match.
     */
    void orderPointsToMatchNodes();
    
    Point<float> startPoint;
private:
    Envelope* envelopeToDisplay;
    
    OwnedArray<DraggableNode> nodeVisuals;
};

#endif /* EnvelopeViewComponent_hpp */
