//
//  AmplitudeEnvComponent.hpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 04/12/2017.
//
//

#ifndef AmplitudeEnvComponent_hpp
#define AmplitudeEnvComponent_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "../EnvelopeComponent.hpp"

/**
 An specialised EnvelopeComponent.
 The AmplitudeEnvComponent draws between nodes starting from the bottom left. It also sets a "release" slider.
 @see EnvelopeComponent
 */
class AmplitudeEnvComponent : public EnvelopeComponent
{
public:
    /**
     Constructor.
     */
    AmplitudeEnvComponent (Array<PartialData>& partialArrayReference);
    /**
     Destructor.
     */
    ~AmplitudeEnvComponent();
        
    /* EnvelopeComponent */
    void newPartialSelected (int partialId) override;
private:
};

#endif /* AmplitudeEnvComponent_hpp */
