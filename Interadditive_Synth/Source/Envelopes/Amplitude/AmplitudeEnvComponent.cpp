//
//  AmplitudeEnvComponent.cpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 04/12/2017.
//
//

#include "AmplitudeEnvComponent.hpp"

AmplitudeEnvComponent::AmplitudeEnvComponent (Array<PartialData>& partialArrayReference) : EnvelopeComponent (partialArrayReference)
{
    windowName.setText ("Amplitude Envelope", dontSendNotification);
    
    param0Label.setText ("Release", dontSendNotification);
    param0.setRange (1.0, 10000.0, 1.0);
    param0.setTextValueSuffix ("ms");
    
    envelopeView.startPoint = Point<float> (0, 0);
}

AmplitudeEnvComponent::~AmplitudeEnvComponent()
{
}

void AmplitudeEnvComponent::newPartialSelected (int partialId)
{    
    for (int i = 0; i < partialArray.size(); i++)
    {
        if (partialArray.getReference (i).getId() == partialId)
        {
            env = partialArray.getReference (i).getAmplitudeEnv();
            envelopeView.newEnvelopeSelected (env);
            break;
        }
    }
    EnvelopeComponent::newPartialSelected (partialId);
}
