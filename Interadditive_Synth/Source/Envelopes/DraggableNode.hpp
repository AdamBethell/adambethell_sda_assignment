//
//  DraggableNode.hpp
//  Interadditive_Synth
//
//  Created by Adam Bethell on 03/01/2018.
//
//

#ifndef DraggableNode_hpp
#define DraggableNode_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "../DraggablePoint.hpp"

/**
 A specialised dragganle point that is square in appearence.
 */
class DraggableNode : public DraggablePoint
{
public:
    /**
     Constructor.
     */
    DraggableNode();
    /**
     Destructor
     */
    ~DraggableNode();
    
    void drawInner (Graphics& g) override;
    void drawOuter (Graphics& g) override;
    
    int currentIndex;
private:
    
    static Array<DraggablePoint*> allNodes;
};

#endif /* DraggableNode_hpp */
