/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "Partials/PartialsComponent.hpp"
#include "Programmer/ProgrammerComponent.hpp"
#include "Envelopes/Amplitude/AmplitudeEnvComponent.hpp"
#include "Envelopes/Frequency/FrequencyEnvComponent.hpp"

/**
 This class holds all GUI elements.
 */
class MainContentComponent   :  public Component,
                                public PartialsComponent::Listener,
                                public ProgrammerComponent::Listener
{
public:
    //==============================================================================
    /**
     Constructor.
     */
    MainContentComponent (Array<PartialData>& partialArrayReference);
    /**
     Destructor.
     */
    ~MainContentComponent();

    /* Component */
    void paint (Graphics&) override;
    void resized() override;
    
    /* PartialsComponent::Listener */
    void selectedPartialChanged (int idOfSelected) override;

    /* ProgrammerComponent::Listener */
    void dataUpdated() override;
    
private:
    PartialsComponent partialsComponent;
    ProgrammerComponent programmerComponent;
    AmplitudeEnvComponent amplitudeEnvComponent;
    FrequencyEnvComponent frequencyEnvComponent;
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainContentComponent)
};
