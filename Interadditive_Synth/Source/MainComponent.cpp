/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainContentComponent::MainContentComponent (Array<PartialData>& partialArrayReference) :
partialsComponent (partialArrayReference),
programmerComponent (partialArrayReference),
amplitudeEnvComponent (partialArrayReference),
frequencyEnvComponent (partialArrayReference)
{
    setSize (1200, 600);
    addAndMakeVisible (&partialsComponent);
    partialsComponent.setListener (this);
    addAndMakeVisible (&programmerComponent);
    programmerComponent.setListener (this);
    addAndMakeVisible (&amplitudeEnvComponent);
    addAndMakeVisible (&frequencyEnvComponent);
}

MainContentComponent::~MainContentComponent()
{
}

void MainContentComponent::paint (Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));
}

void MainContentComponent::resized()
{
    Rectangle<int> bounds = getLocalBounds();
    Rectangle<int> top = bounds.removeFromTop (400);
    partialsComponent.setBounds (top.removeFromLeft (1000).reduced (10));
    programmerComponent.setBounds (top.reduced (10));
    amplitudeEnvComponent.setBounds (bounds.removeFromLeft (600).reduced (10));
    frequencyEnvComponent.setBounds (bounds.reduced (10));
}

void MainContentComponent::selectedPartialChanged (int idOfSelected)
{
    amplitudeEnvComponent.newPartialSelected (idOfSelected);
    frequencyEnvComponent.newPartialSelected (idOfSelected);
}

void MainContentComponent::dataUpdated()
{
    partialsComponent.reload();
}
